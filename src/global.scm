;;;; global.scm - Turing machine global variables

(define comment-character #\;)
(define blank-character   #\_)

(define accept-state "A")
(define reject-state "R")
(define error-state  "E")

(define left-character  #\L)
(define right-character #\R)
(define stay-character  #\S)
